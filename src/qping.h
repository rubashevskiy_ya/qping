#ifndef QPING_H
#define QPING_H

#include <QObject>
#include <QProcess>
#include <QString>
#include <QStringList>
#include <QTimer>

#define DEF_ADRESS "www.google.com"
#define DEF_PACKET_COUNT  5

struct pingResult {
  QString address;
  int packetCount;
  int transmitted;
  int received;
  double timeMin = -1;
  double timeMax = -1;
  double timeavg = -1;
};

enum processStstus {processRun, processFin};
enum processError {ParamError, pingActive, pingNotActive, processTimeout, ProcessUserStop};
enum pingError {UnknownHost, LanError, UnknownError};

class QPing : public QObject
{
  Q_OBJECT
public:
  explicit QPing(QObject *parent = nullptr);
  void setAdress(const QString &adress);
  void setPacketCount(const int &pkcount);
  QString getAdress();
  int getPacketCount();
  void start();
  void stop();
private:
  void getParser(QByteArray Data, bool DataOnStdout);
  QProcess *p_ping = nullptr;
  QTimer *p_timer = nullptr;
  pingResult Result;
  bool p_error = false;
signals:
  void signalErrorProcess(processError);
  void signalErrorPing(pingError);
  void signalResultPing(pingResult);
  void signalStatus(processStstus);
private slots:
  void slotDataOnStdout();
  void slotDataOnStderr();
  void slotStarted();
  void slotFinished();
  void slotTimeout();
};

#endif // QPING_H
