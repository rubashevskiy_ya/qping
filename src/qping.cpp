#include "qping.h"
#include <QDebug>

QPing::QPing(QObject *parent) : QObject(parent)
{
  p_ping = new QProcess(this);
  p_timer = new QTimer(this);
  connect(p_timer, SIGNAL(timeout()), this, SLOT(slotTimeout()));
  connect(p_ping, SIGNAL(readyReadStandardOutput()), SLOT(slotDataOnStdout()));
  connect(p_ping, SIGNAL(readyReadStandardError()), SLOT(slotDataOnStderr()));
  connect(p_ping, SIGNAL(started()), SLOT(slotStarted()));
  connect(p_ping, SIGNAL(finished(int, QProcess::ExitStatus)), SLOT(slotFinished()));
  Result.address = DEF_ADRESS;
  Result.packetCount = DEF_PACKET_COUNT;
}

void QPing::setAdress(const QString &adress)
{
    Result.address = adress;
}

void QPing::setPacketCount(const int &pkcount)
{
    Result.packetCount = pkcount;
}

QString QPing::getAdress()
{
    return Result.address;
}

int QPing::getPacketCount()
{
    return Result.packetCount;
}

void QPing::start()
{  
  if (p_ping->state() != QProcess::NotRunning) {
      emit signalErrorProcess(processError::pingActive);
      return;
    }
  if ((Result.address.isEmpty()) or (Result.packetCount < 1)) {
      emit signalErrorProcess(processError::ParamError);
      return;
    }
  p_error = false;
  p_timer->setInterval(Result.packetCount*2000);
  p_timer->start();
  p_ping->start( "ping", QStringList() << Result.address << "-c" << QString::number(Result.packetCount));
}

void QPing::stop()
{
  if (p_ping->state() == QProcess::NotRunning) {
      emit signalErrorProcess(processError::pingNotActive);
      return;
    }
  p_error = true;
  p_ping->close();
  p_timer->stop();
  emit signalErrorProcess(processError::ProcessUserStop);
}

void QPing::getParser(QByteArray Data, bool Stdout)
{
  QStringList strLines = QString(Data).split("\n"); 
  if (Stdout) {
      QRegExp p_stat("^([0-9]{,2})(\\s)(packets transmitted)\\,(\\s)([0-9]{,2})(\\s)(received)");
      QRegExp s_stat("^(rtt min/avg/max/mdev = )(\\d*\\.\\d+)\\/(\\d*\\.\\d+)\\/(\\d*\\.\\d+)");
     
      foreach (QString l, strLines) {
          if (p_stat.indexIn(l) > -1) {
              Result.transmitted = p_stat.cap(1).toInt();
              Result.received = p_stat.cap(5).toInt();
            }
          if (s_stat.indexIn(l) > -1) {
              Result.timeMin = s_stat.cap(2).toDouble();
              Result.timeavg = s_stat.cap(3).toDouble();
              Result.timeMax = s_stat.cap(4).toDouble();
            }
        }
    }
  else {
      p_error = true;
      QRegExp adress_N_A("^(ping: )(\\S){1,}\\:(\\s)(Неизвестное имя или служба)");
      QRegExp lan_error("^(\\S){1,}(\\s)(Сеть недоступна)");
      foreach (QString l, strLines) {
         if (adress_N_A.indexIn(l) > -1) {
           emit signalErrorPing(pingError::UnknownHost);
           return;
           }
         if (lan_error.indexIn(l) > -1) {
           emit signalErrorPing(pingError::LanError);
           return;
           }
        }
      emit signalErrorPing(pingError::UnknownError);
    }
}

void QPing::slotDataOnStdout()
{
  QByteArray byteArray = p_ping->readAllStandardOutput();
  getParser(byteArray, true);
}

void QPing::slotDataOnStderr()
{
  QByteArray byteArray = p_ping->readAllStandardError();
  getParser(byteArray, false);
}

void QPing::slotStarted()
{
  emit signalStatus(processStstus::processRun);
}

void QPing::slotFinished()
{
  p_timer->stop();
  if (!p_error) emit signalResultPing(Result);
  emit signalStatus(processStstus::processFin);
}

void QPing::slotTimeout()
{
  p_error = true;
  p_timer->stop();
  if (p_ping->state() != QProcess::NotRunning) {
      emit signalErrorProcess(processError::processTimeout);
      p_ping->close();
    }
}

