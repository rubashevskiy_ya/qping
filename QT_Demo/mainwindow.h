#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qping.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
  void on_b_start_ping_clicked();
  void on_b_stop_ping_clicked();
public slots:
  void slotErrorProcess(processError process_error);
  void slotErrorPing(pingError ping_error);
  void slotResultPing(pingResult result);
  void slotStatus(processStstus status);
private:
    Ui::MainWindow *ui;
    QPing * ping = nullptr;
};

#endif // MAINWINDOW_H
