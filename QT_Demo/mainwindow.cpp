#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ping = new QPing(this);
    connect(ping, SIGNAL(signalErrorProcess(processError)), this, SLOT(slotErrorProcess(processError)));
    connect(ping, SIGNAL(signalErrorPing(pingError)), this, SLOT(slotErrorPing(pingError)));
    connect(ping, SIGNAL(signalResultPing(pingResult)), this, SLOT(slotResultPing(pingResult)));
    connect(ping, SIGNAL(signalStatus(processStstus)), this, SLOT(slotStatus(processStstus)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_b_start_ping_clicked()
{
    ui->e_result->clear();
    ping->setAdress(ui->e_adress->text());
    ping->setPacketCount(ui->s_count_pack->value());
    ping->start();
}

void MainWindow::on_b_stop_ping_clicked()
{
    ping->stop();
}

void MainWindow::slotErrorProcess(processError process_error)
{
    switch (process_error) {
      case pingActive: ui->e_result->append("Error: Ping выполняется"); break;
      case pingNotActive: ui->e_result->append("Error: Ping не запущен"); break;
      case ProcessUserStop: ui->e_result->append("Error: Ping остановлен принудительно"); break;
      case ParamError: ui->e_result->append("Error: Ошибка параметров"); break;
      case processTimeout: ui->e_result->append("Error: Ping завершен принудительно"); break;
    }
}

void MainWindow::slotErrorPing(pingError ping_error)
{
    switch (ping_error) {
      case LanError: ui->e_result->append("Error: Сеть недоступна"); break;
      case UnknownHost: ui->e_result->append("Error: Неизвестное имя или служба"); break;
      case UnknownError: ui->e_result->append("Error: Неизвестная ошибка"); break;
    }
}

void MainWindow::slotResultPing(pingResult result)
{
  ui->e_result->append("--------------------------------------");
  ui->e_result->append("Адрес: " + result.address);
  ui->e_result->append("Колличество отправленных пакетов: " + QString::number(result.transmitted));
  ui->e_result->append("Колличество принятых пакетов: " + QString::number(result.received));
  if (int(result.timeMin) != -1) ui->e_result->append("Минимальное время: " + QString::number(result.timeMin));
  if (int(result.timeMax) != -1) ui->e_result->append("Максимальное время: " + QString::number(result.timeMax));
  if (int(result.timeavg) != -1) ui->e_result->append("Среднее время: " + QString::number(result.timeavg));
  ui->e_result->append("--------------------------------------");
}

void MainWindow::slotStatus(processStstus status)
{
    switch (status) {
      case processRun: ui->e_result->append("Ping: Запущен"); break;
      case processFin: ui->e_result->append("Ping: Завершен"); break;
    }
}
